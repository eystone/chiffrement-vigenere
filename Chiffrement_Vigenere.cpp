// Chiffrement Vigenere.cpp
#include <iostream>
#include <string>

std::string str_decalage(std::string strIn, std::size_t ecart) {
	std::string alphabet = std::string("abcdefghijklmnopqrstuvxyz");
	std::string strOut = std::string("");
	
	for (unsigned i(0); i < strIn.size(); i++) {
		//If it's a Maj char make it lower
		if (std::isupper(strIn[i])) strIn[i] = std::tolower(strIn[i]);

		//If char not found (== null pos) put an # to fill the non suported chars
		if (alphabet.find(strIn[i]) == std::string::npos) strOut.push_back((char)35);
		//Or just pick the right char
		else strOut.push_back( alphabet [( alphabet.find(strIn[i]) + ecart) % 26]);
	}
	return strOut;
}

char char_decalage(char charIn, std::size_t ecart) {
	std::string alphabet = std::string("abcdefghijklmnopqrstuvxyz");
	char charOut;

	//If it's a Maj char make it lower
	if (std::isblank(charIn) || std::isupper(charIn)) 
		charIn = std::tolower(charIn);

	//If char not found (== null pos) put an # to fill the non suported chars
	if (alphabet.find(charIn) == std::string::npos) 
		charOut = (char)35;
	//Or just pick the right char
	else charOut = alphabet[(alphabet.find(charIn) + ecart) % 26];
	return charOut;
}

std::string vigenere(std::string str, std::string key) {
	std::string alphabet = std::string("abcdefghijklmnopqrstuvxyz");
	std::string strOut = std::string("");

	for (unsigned i(0); i < str.size(); i++) {
		//If it's a Maj char make it lower
		if (std::isblank(key[i % key.size()]) || std::isupper(key[i % key.size()])) 
			key[i % key.size()] = std::tolower(key[i % key.size()]);

		//If char not found (== null pos) put an @ to fill the non suported chars of the key
		if (alphabet.find(key[i % key.size()]) == std::string::npos) 
			strOut.push_back((char)64);
		//Or just put in strout the char with the move of the key
		else strOut += char_decalage(str[i], alphabet.find ( key[i % key.size()] ) + 1 );
	}
	return strOut;
}

int main() {
	std::string str = "", key = "";
	unsigned ecart(0);
	int choice;

	while (true) {
		std::cout << "Veuillez choisir un mode (0: Cesar, 1: Vigenere) : ";
		std::cin >> choice;

		if (choice == 0) {
			std::cout << "Veuillez Entrer la chaine a modif :\n";
			std::cin >> str;
			std::cout << "Vous avez choisi : " << str << std::endl;

			//if (str.find("quit") != std::string::npos) return 0;

			std::cout << "Veuillez Entrer le nombre de decalage :\n";
			std::cin >> ecart;
			std::cout << "Vous avez choisi : " << ecart << std::endl;

			std::string strOut = str_decalage(str, ecart);
			std::cout << "Apres transformation cela donne : " << strOut << std::endl << std::endl;
		}
		else if (choice == 1){
			std::cout << "Veuillez Entrer la chaine a modif :\n";
			std::cin >> str;
			std::cout << "Vous avez choisi : " << str << std::endl;

			//if (str.find("quit") != std::string::npos) return 0;

			std::cout << "Veuillez la clef de decalage :\n";
			std::cin >> key;
			std::cout << "Vous avez choisi : " << key << std::endl;

			std::string strOut = vigenere(str, key);
			std::cout << "Apres transformation cela donne : " << strOut << std::endl << std::endl;
		}
	}
	return 0;
}